package Task6;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Введите стороны треугольника: ");
        double a= scanner.nextDouble();
        double b= scanner.nextDouble();
        double c= scanner.nextDouble();
        System.out.println("Введите цвет и статус заливки: ");
        String color=scanner.next();
        boolean isFilled=scanner.nextBoolean();
        Triangle triangle=new Triangle(a,b,c,color,isFilled);
        System.out.println("Площадь треуголника: "+triangle.getArea());
        System.out.println("Периметр треугольника: "+triangle.getPerimeter());
        System.out.println("Цвет заливки: "+triangle.getColor());
        System.out.println("Статус заливки: "+triangle.isFilled());
    }
}
