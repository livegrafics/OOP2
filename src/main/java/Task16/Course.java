package Task16;

class Course implements Cloneable {
    private String courseName;
    private String[] students = new String[100];
    private int numberOfStudents;

    public Course(String courseName) {
        this.courseName = courseName;
    }

    public void addStudent(String student) {
        students[numberOfStudents] = student;
        numberOfStudents++;
    }

    public String[] getStudents() {
        return students;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String getCourseName() {
        return courseName;
    }

    public void dropStudent(String student) {
        // Это место оставлено для выполнения задания
    }

    public Object clone() {
        try {
            Course c = (Course) super.clone();
            c.students = new String[100];
            System.arraycopy(students, 0, c.students, 0, 100);
            // Содержимое массива students - строки. Строки являются неизменяемыми,
            // поэтому здесь можно использовать метод arraycopy.
            // Если бы students представлял собой массив изменяемых объектов,
            // то необходимо было выполнять глубокие-глубокие копии.
            c.numberOfStudents = numberOfStudents;
            return c;
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }
}
