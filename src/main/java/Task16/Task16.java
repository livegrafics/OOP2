package Task16;

public class Task16 {
    public static void main(String[] args) {
        Course course1 = new Course("ООП");
        course1.addStudent("S1");
        course1.addStudent("S2");
        course1.addStudent("S3");

        Course course2 = (Course) course1.clone();
        course2.addStudent("S4");
        course2.addStudent("S5");
        course2.addStudent("S6");

        System.out.println("Студенты на course1: ");
        for (int i = 0; i < course1.getNumberOfStudents(); i++)
            System.out.print(course1.getStudents()[i] + " ");
        System.out.println("\nСтуденты на course1: ");
        for (int i = 0; i < course2.getNumberOfStudents(); i++)
            System.out.print(course2.getStudents()[i] + " ");
    }
}
