package Task5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        boolean isIncorrect;
        int a = 0;
        int b = 0;
        do {
            try {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Введите числа a и b: ");
                a = scanner.nextInt();
                b = scanner.nextInt();
                isIncorrect = false;
            } catch (InputMismatchException e) {
                System.out.println("Некорректный ввод");
                isIncorrect = true;
            }
        } while (isIncorrect);
        System.out.println("Сумма чисел " + a + " и " + b + " = " + (a + b));
    }
}
