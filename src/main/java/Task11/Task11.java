package Task11;

public class Task11 {
    public static void main(String[] args) {
        GeometricObject[] geometricObject=new GeometricObject[5];
        geometricObject[0] = new Circle (100);
        geometricObject[1] = new Rectangle(100,123);
        geometricObject[2] = new Square(123);
        geometricObject[3] =new Circle(123);
        geometricObject[4]=new Square(12);
        for (int i=0; i<5;i++){
            System.out.println(geometricObject[i].toString());
            System.out.println("Площадь фигуры = "+geometricObject[i].getArea());
            if (geometricObject[i] instanceof Square){
                ((Square) geometricObject[i]).howToColor();
            }
            System.out.println();
        }
    }
}
