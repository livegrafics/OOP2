package Practicum.Side2;

public class Octagon extends GeometricObject implements Cloneable,Comparable<Octagon> {

    private double side;

    public Octagon() {
        this.side=0;
    }

    public Octagon(double side) {
        this.side = side;
    }

    @Override
    public double getArea() {
        return (2+4/Math.sqrt(2))*side*side;
    }

    @Override
    public double getPerimeter() {
        return 8*side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        try {
            return super.clone();
        }catch (CloneNotSupportedException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int compareTo(Octagon o) {
        if (this.getArea()==o.getArea()){
            return 0;
        }else if (this.getArea()>o.getArea()){
            return 1;
        }else{
            return -1;
        }
    }
}
