package Practicum.Side1;

import java.util.Scanner;

public class Practicum1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numerator = scanner.nextInt();
        int denominator = scanner.nextInt();
        Fraction fraction = null;
        try {
            fraction = new Fraction(numerator, denominator);
        } catch (NullDenominatorException e) {
            System.out.println(e.getMessage());
            ;
        }
        if (fraction != null) {
            System.out.println("Сформирована дробь: " + fraction.getFraction());
        }
    }
}
