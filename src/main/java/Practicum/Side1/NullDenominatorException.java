package Practicum.Side1;

public class NullDenominatorException extends Exception {
    public NullDenominatorException(String message) {
        super(message);
    }
}
