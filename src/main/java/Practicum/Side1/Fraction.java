package Practicum.Side1;

public class Fraction {
    public final int numerator;
    public final int denominator;
    private String fraction;

    public Fraction(int numerator, int denominator) throws NullDenominatorException {
        this.numerator = numerator;
        this.denominator = denominator;
        if (denominator == 0) throw new NullDenominatorException("Нулевой знаменатель");
        else {
            fraction = numerator + "/" + denominator;
        }
    }

    public String getFraction() {
        return fraction;
    }
}
