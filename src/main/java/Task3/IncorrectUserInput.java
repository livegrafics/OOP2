package Task3;

public class IncorrectUserInput extends Exception{
    public IncorrectUserInput(String message) {
        super(message);
    }
}
