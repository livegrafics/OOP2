package Task9;

public class Task9 {
    public static void main(String[] args) {
        Circle circle1=new Circle(125.001);
        Circle circle2=new Circle(125);
        GeometricObject.max(circle1,circle2);
        System.out.println();
        Rectangle rectangle1=new Rectangle(24,7.01);
        Rectangle rectangle2=new Rectangle(7,24);
        GeometricObject.max(rectangle1,rectangle2);
    }
}
