package Task9;

public abstract class GeometricObject implements Comparable<GeometricObject> {
    private String color = "белый";
    private boolean filled;
    private java.util.Date dateCreated;

    /** Создает по умолчанию заданную геометрическую фигуру */
    protected GeometricObject() {
        dateCreated = new java.util.Date();
    }

    /** Создает геометрическую фигуру с указанными цветом и заливкой */
    protected GeometricObject(String color, boolean filled) {
        dateCreated = new java.util.Date();
        this.color = color;
        this.filled = filled;
    }

    /** Возвращает цвет */
    public String getColor() {
        return color;
    }

    /** Присваивает новый цвет */
    public void setColor(String color) {
        this.color = color;
    }

    /** Возвращает заливку. Поскольку поле filled типа boolean,
     *  getter-метод называется isFilled */
    public boolean isFilled() {
        return filled;
    }

    /** Присваивает новую заливку */
    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    /** Возвращает dateCreated */
    public java.util.Date getDateCreated() {
        return dateCreated;
    }

    @Override
    public String toString() {
        return "создано " + dateCreated + "\nцвет: " + color +
                " и заливка: " + filled;
    }

    /** Абстрактный метод getArea */
    public abstract double getArea();

    /** Абстрактный метод getPerimeter */
    public abstract double getPerimeter();

    @Override
    public int compareTo(GeometricObject o) {
        if (this.getArea()==o.getArea()){
            return 0;
        }
        else if (getArea()<o.getArea()){
            return -1;
        }else{
            return 1;
        }
    }
    public static void max (GeometricObject o1, GeometricObject o2){
        System.out.println("Объект1: "+ o1.toString());
        System.out.println("Объект2: " + o2.toString());
        if (o1.compareTo(o2)==0) {
            System.out.println("Фигуры равны");
        }else if (o1.compareTo(o2)==-1){
            System.out.println("Фигура1 меньше фигуры2");
        }else{
            System.out.println("Фигура1 больше фигуры2");
        }
    }
}
