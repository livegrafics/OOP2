package Task12;

public class Task12 {
    public static void main(String[] args) {
        MyStack stack = new MyStack();
        stack.push("S1");
        stack.push("S2");
        stack.push("S");

        MyStack stack2 = (MyStack) (stack.clone());
        stack2.push("S1");
        stack2.push("S2");
        stack2.push("S");

        System.out.println(stack.getSize());
        System.out.println(stack2.getSize());
    }
}
