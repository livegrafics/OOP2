package Task4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("Введите длины сторон треугольника");
        Scanner scanner=new Scanner(System.in);
        int side1=scanner.nextInt();
        int side2=scanner.nextInt();
        int side3=scanner.nextInt();
        Triangle triangle;
        try{
            triangle=new Triangle(side1,side2,side3);
        }catch (IllegalTriangleException e){
            System.out.println(e.getMessage());
        }
    }
}
