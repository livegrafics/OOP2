package Task15;

public class Task15 {
    public static void main(String[] args) {
        GeometricObject[] geometricObjects=new GeometricObject[4];
        geometricObjects[0]=new Circle(100);
        geometricObjects[1]=new Circle(12);
        geometricObjects[2]=new Rectangle(10,123);
        geometricObjects[3]=new Rectangle(110,23);
        System.out.println(sumArea(geometricObjects));
    }
    public static double sumArea(GeometricObject[] a){
        double summArea=0;
        for (int i=0;i< a.length;i++){
            summArea=summArea+a[i].getArea();
        }
        return summArea;
    }
}
