package Task10;

public class ComparableCircle extends Circle implements Comparable<ComparableCircle> {
    public ComparableCircle(double radius) {
        super(radius);
    }

    @Override
    public int compareTo(ComparableCircle o) {
        if (this.getArea() == o.getArea()) {
            return 0;
        } else if (this.getArea() > o.getArea()) {
            return 1;
        } else return -1;
    }


    @Override
    public String toString() {
        return "Circle {radius="+getRadius()+"}";
    }
}
