package Task10;

public class Task10 {
    public static void main(String[] args) {
        ComparableCircle circle1 = new ComparableCircle(121.99);
        ComparableCircle circle2 = new ComparableCircle(122);
        if (circle1.compareTo(circle2) == 0) {
            System.out.println("Фигуры равны");
        } else if (circle1.compareTo(circle2) == 1) {
            System.out.println("Наибольшая фигура: " + circle1.toString());
        } else {
            System.out.println("Наибольшая фигура: " + circle2.toString());
        }
    }
}
