package Task1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        String[] months = {"январь", "февраль", "март", "апрель", "май",
                "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};
        int[] dom = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        Scanner scanner = new Scanner(System.in);
        int userMonth = scanner.nextInt();
        try {
            System.out.println("В месяце " + months[userMonth - 1] + " " + dom[userMonth - 1] + " день(ней)");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Введено недопустимое число!!!");
            ;
        }
    }
}
